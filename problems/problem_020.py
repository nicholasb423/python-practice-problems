# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # get length of the attendees_list
    num_attendees = len(attendees_list)
    # get length of the members_list
    num_members = len(members_list)
    # if members_list divided by attendees_list is greater than or equal to 0.5
    if num_members / num_attendees >= 0.5:
        # return true
        return True
    else:
        return False
