# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    # Check if the number is divisable by 3
        # Return "fizz"
    if number % 3 == 0:
        return "fizz"
    # If it is not divisable by 3
        # return the number
    else:
        return number
