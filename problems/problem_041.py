# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    # create an empty result list variable "result = []"
    result = []
    # Iterate over csv_line
    for i in csv_lines:
        # assign a variable to the split items " split_items = i.split(",") "
        split_items = i.split(",")
        # create a variable for the sum " sum = 0 "
        sum = 0
        # iterate over the split_items
        for j in split_items:
            # create a variable for the integer value
            value = int(j)
            # add the value to the sum
            sum += value
        # append the result list with the sum
        result.append(sum)
    return result
