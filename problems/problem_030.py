# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # If the list <= 1
    if len(values) <= 1:
        #return None
        return None
    # Create a largest variable
    # Create a second_largest variable
    largest = values[0]
    second_largest = values[0]
    # iterate over the list
    for num in values:
        # If num > largest
        if num > largest:
            # second_largest = largest
            second_largest = largest
            # largest = num
            largest = num
        # Elif num > second_largest
        elif num > second_largest:
            # second_largest = num
            second_largest = num
        # return second_largest
    return second_largest
