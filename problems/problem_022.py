# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    # Create a gear list
    gear = []
    # Check if it is a work day and not sunny
    if not is_sunny and is_workday:
        # Bring "umbrella"
        gear.append("umbrella")
    # If it is a work day
    elif is_workday:
        # Bring "laptop"
        gear.append("laptop")
    # If not a workday
    else:
        # Bring "surfboard"
        gear.append("surfboard")
